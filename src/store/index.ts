import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { composeWithDevTools } from 'redux-devtools-extension';
import feedsReducer from './reducers/feedsReducer';
import rootSaga from './sagas';

const rootReducer = feedsReducer;

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: [
    'selectedFeedData',
  ]
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default store;