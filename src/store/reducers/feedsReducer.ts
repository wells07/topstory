import { filter } from 'lodash';

import FEEDS from './default-data/FEEDS';
import { FeedActionTypes } from './types'

export interface Feeds {
  name: string,
  url: string,
  isFavourite: Boolean,
};

export interface FeedItems {
  title: string,
  description: string,
  published: string,
  links: {
    url: string,
  }[],
};

export interface SelectedFeedData {
  title: string,
  url: string,
  description: string,
  feedItems: FeedItems[], 
};

export interface FeedsState {
  loading: Boolean,
  error: Boolean,
  feeds: Feeds[],
  selectedFeedData: SelectedFeedData,
};

const initialState = {
  loading: false,
  error: false,
  feeds: FEEDS,
  selectedFeedData: {
    title: '',
    url: '',
    description: '',
    feedItems: []
  },
};

const toggleFavourite = (list: Feeds[], targetUrl: string, newValue: Boolean) : Feeds[] => {
  const selectedRSS = filter([...list], ['url', targetUrl])[0];
  selectedRSS.isFavourite = newValue;
  const updatedList = filter([...list], o => o.url !== targetUrl);
  updatedList.push(selectedRSS);
  return updatedList;
};

const feedsReducer = (state: FeedsState = initialState, action: FeedActionTypes) : FeedsState => {
  switch(action.type) {
    case 'ADD_RSS_FEED_TO_FAVOURITES': {
      const newFeedList = toggleFavourite(state.feeds, action.url, true);
      return { ...state, feeds: newFeedList };
    }
    case 'REMOVE_RSS_FEED_FROM_FAVOURITES': {
      const newFeedList = toggleFavourite(state.feeds, action.url, false);
      return { ...state, feeds: newFeedList};
    }
    case 'RSS_FEED_SUCCESS': {
      return { ...state, selectedFeedData: action.feedData, loading: false };
    }
    case 'RSS_FEED_LOADING': {
      return { ...state, loading: true, error: false };
    }
    case 'RSS_FEED_FAILURE': {
      return { ...state, loading: false, error: true };
    }
    default:
    return state;
  }
};

export default feedsReducer;
