export default [
  {
    name: 'Top Stories',
    url: 'https://rss.dw.com/rdf/rss-en-top',
    isFavourite: true,
  },
  {
    name: 'German News English',
    url: 'https://rss.dw.com/rdf/rss-en-ger',
    isFavourite: true,
  },
  {
    name: 'World News',
    url: 'https://rss.dw.com/rdf/rss-en-world',
    isFavourite: false,
  },
  {
    name: 'EU News',
    url: 'https://rss.dw.com/rdf/rss-en-eu',
    isFavourite: false,
  },
  {
    name: 'Business News',
    url: 'https://rss.dw.com/rdf/rss-en-bus',
    isFavourite: false,
  },
];