import { SelectedFeedData } from './feedsReducer';

export const GET_RSS_FEED = 'GET_RSS_FEED';
export const RSS_FEED_LOADING = 'RSS_FEED_LOADING';
export const RSS_FEED_SUCCESS = 'RSS_FEED_SUCCESS';
export const RSS_FEED_FAILURE = 'RSS_FEED_FAILURE';
export const ADD_RSS_FEED_TO_FAVOURITES = 'ADD_RSS_FEED_TO_FAVOURITES';
export const REMOVE_RSS_FEED_FROM_FAVOURITES = 'REMOVE_RSS_FEED_FROM_FAVOURITES';

export interface getRSSFeedActionType {
  type: typeof GET_RSS_FEED,
  url: string,
}

interface setLoadingActionType {
  type: typeof RSS_FEED_LOADING,
}

interface setGetSuccessActionType {
  type: typeof RSS_FEED_SUCCESS,
  feedData: SelectedFeedData,
}

interface setGetFailureActionType {
  type: typeof RSS_FEED_FAILURE,
}

interface AddFeedToFavouritesActionType {
  type: typeof ADD_RSS_FEED_TO_FAVOURITES,
  url: string,
}

interface RemoveFeedFromFavouritesActionType {
  type: typeof REMOVE_RSS_FEED_FROM_FAVOURITES,
  url: string,
}

export type FeedActionTypes = 
getRSSFeedActionType |
setLoadingActionType |
setGetSuccessActionType |
setGetFailureActionType |
AddFeedToFavouritesActionType |
RemoveFeedFromFavouritesActionType;