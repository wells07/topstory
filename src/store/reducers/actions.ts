import {
  GET_RSS_FEED,
  RSS_FEED_LOADING,
  RSS_FEED_SUCCESS,
  RSS_FEED_FAILURE,
  ADD_RSS_FEED_TO_FAVOURITES,
  REMOVE_RSS_FEED_FROM_FAVOURITES,
  FeedActionTypes,
} from './types'

import { SelectedFeedData } from './feedsReducer';

export function getRSSFeedAction(url: string): FeedActionTypes {
  return {
    type: GET_RSS_FEED,
    url,
  }
}

export function setLoadingAction(): FeedActionTypes {
  return {
    type: RSS_FEED_LOADING,
  }
}

export function setGetSuccessAction(feedData: SelectedFeedData): FeedActionTypes {
  return {
    type: RSS_FEED_SUCCESS,
    feedData,
  }
}

export function setGetFailureAction(): FeedActionTypes {
  return {
    type: RSS_FEED_FAILURE,
  }
}

export function addFeedToFavouritesAction(url: string): FeedActionTypes {
  return {
    type: ADD_RSS_FEED_TO_FAVOURITES,
    url,
  }
}

export function removeFeedFromFavouritesAction(url: string): FeedActionTypes {
  return {
    type: REMOVE_RSS_FEED_FROM_FAVOURITES,
    url,
  }
}