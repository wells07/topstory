import { all, takeLatest } from 'redux-saga/effects';
import { getRSSfeedSaga } from './feedsSaga';
import { GET_RSS_FEED } from '../reducers/types';

export default function* rootSaga() {
  yield all([
    takeLatest(GET_RSS_FEED, getRSSfeedSaga),
  ]);
}
