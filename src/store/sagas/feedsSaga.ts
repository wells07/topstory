import { call, put } from 'redux-saga/effects'
import axios from 'axios';
import { parse } from 'react-native-rss-parser';

import { getRSSFeedActionType } from '../reducers/types';
import { setLoadingAction, setGetSuccessAction, setGetFailureAction } from '../reducers/actions';
import { SelectedFeedData } from '../reducers/feedsReducer';

export function* getRSSfeedSaga({ url }: getRSSFeedActionType) {
    // TODO Check if device is online first
  try {

    yield put(setLoadingAction());

    const result = yield call(axios.get, url, { responseType: 'text' });
    
    const {
      title,
      description,
      items: feedItems,
    } = yield parse(result?.data) || {};
    
    const formattedData : SelectedFeedData = {
      title,
      description,
      url,
      feedItems,
    };

    yield put(setGetSuccessAction(formattedData));

  } catch (e) {
    yield put(setGetFailureAction());
    console.log(e);
  }
}
