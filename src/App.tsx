import React, { FC } from 'react';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { PersistGate } from 'redux-persist/integration/react';
import store, { persistor } from './store';
import { Home, Feed } from './ui';

const Stack = createStackNavigator();

const App: FC = () => (
  <NavigationContainer>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Feed" component={Feed} />
      </Stack.Navigator>
      </PersistGate>
    </Provider>
  </NavigationContainer>
);

export default App;
