import React, { FC } from 'react';
import { Button, ActivityIndicator, FlatList, Linking } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { sortBy } from 'lodash';
import moment from 'moment';

import { Container, TextSmall, Wrapper, TextHeader, Link } from './styles'
import { FeedsState, FeedItems } from '../../store/reducers/feedsReducer';
import { getRSSFeedAction } from '../../store/reducers/actions';
import { TextRegular } from '../home/styles';

interface FeedProps {
  route: {
    params: {
      url: string,
    },
  };
}

interface RenderItem {
  item: FeedItems,
}

const Feed: FC<FeedProps> = ({ route }) => {

  const { url } =  route.params;

  const loading = useSelector<FeedsState, FeedsState['loading']>(({ loading }) => loading);
  const error = useSelector<FeedsState, FeedsState['error']>(({ error }) => error);
  const feedData = useSelector<FeedsState, FeedsState['selectedFeedData']>(({ selectedFeedData }) => selectedFeedData);

  const dispatch = useDispatch();

  const handleGetRss = () => dispatch(getRSSFeedAction(url));

  const goToUrl = (link: string) => {
    Linking.openURL(link)
    .catch((e: string) => console.error(e))
  }

  const sortByDate = () => {
    const reducer = (acc: any, item: FeedItems) => {
      const newItem = { ...item, published: moment(item.published).unix() }
      return [...acc, { ...newItem }];
    }
    return sortBy(
      feedData.feedItems.reduce(reducer, []),
      ['published']
    ).reverse();
  };

  if (loading) return (
    <Container>
      <ActivityIndicator size="small" color="#0000ff" />
    </Container>
  );

  if (error) return (
    <Container>
      <TextSmall>Oops something went wrong</TextSmall>
      <Button onPress={handleGetRss} title='try again' />
    </Container>
  );

  const HeaderData = () => (
    <Wrapper>
      <TextHeader>{feedData?.title}</TextHeader>
      <TextRegular>{feedData?.description}</TextRegular>
    </Wrapper>
  );

  const renderItem = ({ item } : RenderItem) => (
    <Link onPress={() => goToUrl(item?.links[0]?.url)}>
      <TextRegular>{item?.title}</TextRegular>
      <TextSmall>{item?.description}</TextSmall>
      <TextSmall>Dated: {moment(item?.published, 'X').format("MMM DD YYYY")}</TextSmall>
    </Link>
  );
  
  return (
    <Container>
      <FlatList
        data={sortByDate()}
        renderItem={renderItem}
        keyExtractor={({ title }) => title}
        ListHeaderComponent={HeaderData}
      />
    </Container>
  );
};

export default Feed;
