import styled from '@emotion/native';

export const Container = styled.View`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  padding: 5px;
  background-color: whitesmoke;
`;

export const Wrapper = styled.View`
  display: flex;
  align-items: center;
  padding: 5px;
  background-color: #fff;
`;

export const TextSmall = styled.Text`
  font-size: 12px;
`;

export const TextHeader = styled.Text`
  font-weight: bold;
  font-size: 18px;
`;

export const Link = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  padding: 15px 10px;
  margin-bottom: 10px;
  background-color: #fff;
`;
