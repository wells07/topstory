import React, { FC } from 'react';
import { Card, TextRegular, ClickBox, SmallClickBox, FakeGrayIcon, FakeGreenIcon } from './styles';

export interface ListeItemProps {
  name: string,
  url: string,
  isFavourite: Boolean,
  toggleFavourite: (url: string) => void,
  goToFeed: (url: string) => void,
}

const ListItem: FC<ListeItemProps> = ({
  name, url, isFavourite, toggleFavourite, goToFeed
}) => {

  const handleItemPress = () => goToFeed(url);
  const handleTogglePress = () => toggleFavourite(url);

  return (
    <Card>
      <ClickBox onPress={handleItemPress}>
        <TextRegular>{name}</TextRegular>
      </ClickBox>
      <SmallClickBox onPress={handleTogglePress}>
        {isFavourite ? <FakeGreenIcon>★</FakeGreenIcon> : <FakeGrayIcon>★</FakeGrayIcon>}
      </SmallClickBox>
    </Card>
  );
};

export default ListItem;
