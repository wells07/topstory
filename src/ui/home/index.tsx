import React, { FC, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView, FlatList } from 'react-native';
import { sortBy } from 'lodash';

import { Container } from './styles';
import { FeedsState, Feeds } from '../../store/reducers/feedsReducer';
import { addFeedToFavouritesAction, removeFeedFromFavouritesAction, getRSSFeedAction } from '../../store/reducers/actions';
import ListItem from './list-item';

interface HomeProps {
  navigation: {
    navigate: (
      route: string,
      param: { url: string }
    ) => void,
  },
}

interface RenderItem {
  item: Feeds,
}

const Home: FC<HomeProps> = ({ navigation }) => {

  const [rssfeeds, setRssfeeds] = useState(
    useSelector<FeedsState, FeedsState['feeds']>(({ feeds = [] }) => feeds)
  );

  const rssSortedbyFavourite: Feeds[] = sortBy(rssfeeds, o => !o.isFavourite);

  useEffect(() => {}, [rssfeeds]);

  const dispatch = useDispatch();

  const addRSSFeedToFavourites = (url: string) => {
    dispatch(addFeedToFavouritesAction(url));
  };

  const removeRSSFeedFromFavourites = (url: string) => {
    dispatch(removeFeedFromFavouritesAction(url));
  };

  const renderItem = ({ item } : RenderItem) => {
    const { name, url, isFavourite } = item;
    const toggleFavourite = isFavourite ? removeRSSFeedFromFavourites : addRSSFeedToFavourites;
    const goToFeed = () => {
      dispatch(getRSSFeedAction(url));
      setTimeout(() => navigation.navigate('Feed', { url }), 100)
    };

    return (
      <ListItem
        name={name}
        url={url}
        isFavourite={isFavourite}
        toggleFavourite={toggleFavourite}
        goToFeed={goToFeed}
      />
    );
  };

  return (
    <SafeAreaView>
      <Container>
        <FlatList
          data={rssSortedbyFavourite}
          renderItem={renderItem}
          keyExtractor={({ url }) => url}
        />
      </Container>
    </SafeAreaView>
  );
};

export default Home;
