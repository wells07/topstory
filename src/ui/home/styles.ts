import styled from '@emotion/native';

export const Container = styled.View`
  display: flex;
  padding: 5px;
  background-color: whitesmoke;
`;

export const Card = styled.View`
  display: flex;
  padding: 10px 5px;
  margin-bottom: 5px;
  border-bottom-width: 1px;
  border-color: #ddd;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ClickBox = styled.TouchableOpacity`
  padding: 5px;
  width: 80%;
  border-right-width: 5px;
  border-color: #ddd;
`;

export const SmallClickBox = styled(ClickBox)`
  width: 18%;
  border-right-width: 0;
  align-items: center;
`;

export const TextHeader = styled.Text`
  font-weight: bold;
  font-size: 18px;
`;

export const TextRegular = styled.Text`
  font-size: 16px;
`;

export const TextSmall = styled.Text`
  font-size: 12px;
`;

type FakeIconProps = {
  isFavourite: Boolean,
}

export const FakeGrayIcon = styled.Text`
  font-weight: bold;
  font-size: 30px;
  color: #ddd;
`;

export const FakeGreenIcon = styled(FakeGrayIcon)`
  color: green;
`;

